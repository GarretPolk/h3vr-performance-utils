# H3VR Performance Utils #

These are scripts for improving performance of modded maps in the game Hotdogs, Horseshoes, and Hand Grenades (H3VR).

### How do I get set up? ###

You need to compile this code into a DLL and add that DLL to your Unity Editor AND export it with your map. I intentionally didn't provide a DLL because I've run into issues with the current H3VR mod system loading the same DLL in multiple maps.

https://github.com/Nolenz/WurstMod/wiki/Creating-Custom-Scripts
https://docs.unity3d.com/550/Documentation/Manual/scriptsinassetbundles.html
http://t-machine.org/index.php/2014/05/24/how-to-tidy-your-unity-code-by-putting-it-in-a-dll/

### How do they work? ###

I have a long explanation video here:
https://www.youtube.com/watch?v=zaIOFqZUVmI&t=4434s

There are also comments in each file.

### Questions? ###

Ask questions in the H3VR Discord in the "modding-help" room.
https://discord.gg/Hggg7wh

## License ##
https://creativecommons.org/publicdomain/zero/1.0/

License: (CC0 1.0 Universal) You 're free to use these game assets in any project, personal or commercial. There's no need to ask permission before using these. Giving attribution is not required, but is greatly appreciated!
The original author is Garret Polk.