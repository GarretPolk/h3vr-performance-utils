﻿/// https://creativecommons.org/publicdomain/zero/1.0/
/// License: (CC0 1.0 Universal) You 're free to use these game assets in any project, 
/// personal or commercial. There's no need to ask permission before using these.
/// Giving attribution is not required, but is greatly appreciated!
/// The original author is Garret Polk
/// https://bitbucket.org/GarretPolk/h3vr-performance-utils/src/main/

using UnityEngine;

/// <summary>
/// Turn on/off Behaviours (script components) based on 
/// whether Unity occlusion culls it.
/// https://blogs.unity3d.com/2016/12/20/unitytips-particlesystem-performance-culling/
/// </summary>
public class CustomParticleCulling : MonoBehaviour
{
    [Tooltip("If this sphere is seen, or the camera is inside, the particle systems are enabled.")]
    public float cullingRadius = 10;
    public ParticleSystem target;

    // Unity's culling group
    CullingGroup m_CullingGroup;

    [Tooltip("The particle renderers we want to enable/disable")]
    public Renderer[] m_ParticleRenderers;

    // Used for culling initialization
    Camera cameraMain;

    void Update()
    {
        // Wait until we have a valid camera/player
        if ( cameraMain == null )
        {
            cameraMain = Camera.main;

            if (cameraMain != null)
            {
                // Ready
                Init();
            }
        }
    }

    void Init()
    {
        if (m_CullingGroup == null)
        {
            m_CullingGroup = new CullingGroup();
            m_CullingGroup.targetCamera = Camera.main;
            m_CullingGroup.SetBoundingSpheres(new[] { new BoundingSphere(transform.position, cullingRadius) });
            m_CullingGroup.SetBoundingSphereCount(1);
            m_CullingGroup.onStateChanged += OnStateChanged;

            // We need to start in a culled state
            Cull(m_CullingGroup.IsVisible(0));
        }

        m_CullingGroup.enabled = true;
    }

    void OnEnable()
    {
        if (m_CullingGroup != null)
        {
            m_CullingGroup.enabled = true;
            target.Play(true);
            SetRenderers(true);
        }
    }

    void OnDisable()
    {
        if (m_CullingGroup != null)
        {
            m_CullingGroup.enabled = false;
            target.Pause(true);
            SetRenderers(false);
        }
    }

    /// <summary>
    /// Release memory
    /// </summary>
    void OnDestroy()
    {
        if (m_CullingGroup != null)
            m_CullingGroup.Dispose();
    }

    /// <summary>
    /// Callback from Unity's occlusion culling. State changed.
    /// </summary>
    /// <param name="sphere"></param>
    void OnStateChanged(CullingGroupEvent sphere)
    {
        Cull(sphere.isVisible);
    }

    /// <summary>
    /// Pause/play the particle systems and show/hide the renderers
    /// </summary>
    /// <param name="enable"></param>
    void Cull(bool visible)
    {
        if (visible)
        {
            if (target != null)
                target.Play(true);
            SetRenderers(true);

        }
        else
        {
            if (target != null)
                target.Pause(true);
            SetRenderers(false);
        }
    }

    void SetRenderers(bool enable)
    {
        if (m_ParticleRenderers != null)
        {
            // We also need to disable the renderer to prevent drawing the
            // particles, such as when occlusion occurs.
            for (int i = m_ParticleRenderers.Length - 1; i > -1; --i)
            {
                m_ParticleRenderers[i].enabled = enable;
            }                
        }
    }

    void OnDrawGizmosSelected()
    {
        if (enabled)
        {
            // Draw gizmos to show the culling sphere.
            Color col = Color.yellow;
            if (m_CullingGroup != null && !m_CullingGroup.IsVisible(0))
                col = Color.gray;

            Gizmos.color = col;
            Gizmos.DrawWireSphere(transform.position, cullingRadius);
        }
    }
}